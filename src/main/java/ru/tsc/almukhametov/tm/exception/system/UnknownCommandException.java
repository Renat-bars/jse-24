package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@Nullable final String command) {
        super("Incorrect command. Command ``" + command + "`` was not founded. " +
                "Use ``" + TerminalConst.HELP + "`` for display list of terminal commands");
    }
}
