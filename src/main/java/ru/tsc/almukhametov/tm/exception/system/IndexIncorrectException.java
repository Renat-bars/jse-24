package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error. Index is incorrect.");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

    public IndexIncorrectException(@NotNull final String value) {
        super("Error. This value ``" + value + "`` is not number.");
    }

}
