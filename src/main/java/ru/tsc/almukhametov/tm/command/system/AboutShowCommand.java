package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public final class AboutShowCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.ABOUT;
    }

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ABOUT;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.ABOUT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Renat Almukhametov");
        System.out.println("E-MAIL: rralmukhametov@tsconsulting.com");
    }

}
