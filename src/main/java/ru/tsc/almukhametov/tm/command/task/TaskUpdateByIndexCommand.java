package ru.tsc.almukhametov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.TASK_UPDATE_BY_INDEX;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.TASK_UPDATE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!serviceLocator.getTaskService().existByIndex(userId, index)) throw new EmptyIndexException();
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdate = serviceLocator.getTaskService().updateByIndex(userId, index, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
