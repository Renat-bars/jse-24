package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IAuthenticationService getAuthenticationService();

    @NotNull
    IUserService getUserService();

}
